package com.example.test_ws;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import static com.example.test_ws.MainActivity.APP_NAME;
import static com.example.test_ws.MainActivity.APP_NAME_COLOR;

public class Text_reg extends AppCompatActivity {

    public static final String APP_TEXT = "mytextset";
    public static final String APP__TEXT_EDIT = "text";
    SharedPreferences mColor;
    SharedPreferences mText;


    int color;
    LinearLayout back;
    Button next;

    EditText text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text_xml);
        back = findViewById(R.id.background_1);
        next.findViewById(R.id.button);
        text = findViewById(R.id.text);
        mColor = getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);
        mText = getSharedPreferences(APP_TEXT, Context.MODE_PRIVATE);


        color = mColor.getInt(APP_NAME_COLOR,0);
        if(color == 1){
            back.setBackgroundResource(R.color.red);
        }
        if(color == 2){
            back.setBackgroundResource(R.color.blue);

        }
        if(color == 3){
            back.setBackgroundResource(R.color.yellow);

        }
        if(color == 4){
            back.setBackgroundResource(R.color.green);

        }
        if(color == 5) {
            back.setBackgroundResource(R.color.pink);

        }
        if(color == 6){
            back.setBackgroundResource(R.color.black);

        }
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = mColor.edit();
                editor.putString(APP_NAME_COLOR, String.valueOf(text.getText()));
                editor.apply();
                startActivity(new Intent(Text_reg.this, Main.class));
            }
        });




    }
}
