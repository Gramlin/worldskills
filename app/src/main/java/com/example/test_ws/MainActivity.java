package com.example.test_ws;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import static com.example.test_ws.R.drawable.ic_launcher_background;

public class MainActivity extends AppCompatActivity {

    public static final String APP_NAME = "mysettings";
    public static final String APP_NAME_COLOR = "color";

    SharedPreferences mColor;

    LinearLayout background;
    int color = 0;
    Button next;
    Button red;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mColor = getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);

        background = findViewById(R.id.background);
        next = findViewById(R.id.button8);
        red = findViewById(R.id.button2);

       next.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               SharedPreferences.Editor editor = mColor.edit();
               editor.putInt(APP_NAME_COLOR, color);
               editor.apply();
               startActivity(new Intent(MainActivity.this,Text_reg.class));

           }
       });

    }
    public void red(View view){
        background.setBackgroundResource(R.color.red);
        color = 1;
    }
    public void blue(View view){
        background.setBackgroundResource(R.color.blue);
        color = 2;
    }
    public void yellow(View view){
        background.setBackgroundResource(R.color.yellow);
        color = 3;
    }
    public void green(View view){
        background.setBackgroundResource(R.color.green);
        color = 4;
    }
    public void pink(View view){
        background.setBackgroundResource(R.color.pink);
        color = 5;
    }
    public void black(View view){
        background.setBackgroundResource(R.color.black);
        color = 6;
    }
}
